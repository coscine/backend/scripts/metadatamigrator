﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace MetadataMigrator.Logging
{
    [LayoutRenderer("assembly-name")]
    public class AssemblyNameLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var assembly = typeof(Program).Assembly.GetName();
            if (assembly is not null)
            {
                builder.Append(assembly.Name);
            }
            else
            {
                builder.Append(new Guid().ToString().Take(8));
            }
        }
    }
}
