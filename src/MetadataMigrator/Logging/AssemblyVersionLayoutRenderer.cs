﻿using NLog;
using NLog.LayoutRenderers;
using System.Text;

namespace MetadataMigrator.Logging
{
    [LayoutRenderer("assembly-version")]
    public class AssemblyVersionLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var assembly = typeof(Program).Assembly.GetName();
            if (assembly is not null && assembly.Version is not null)
            {
                builder.Append(assembly.Version.ToString(3));
            }
            else
            {
                builder.Append(new Version().ToString(3));
            }
        }
    }
}
