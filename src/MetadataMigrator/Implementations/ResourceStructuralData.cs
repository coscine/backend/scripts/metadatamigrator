using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.Util;
using Coscine.Metadata;
using Coscine.Metadata.Util;
using Microsoft.Extensions.Logging;
using VDS.RDF;
using VDS.RDF.Query;

namespace MetadataMigrator.Implementations
{
    public class ResourceStructuralData : StructuralData<Resource, ResourceModel>
    {
        private readonly Dictionary<string, string> _targetClassMap = new Dictionary<string, string>();
        private readonly int QUERY_LIMIT = 1000;

        // Override to also receive deleted resources
        public override IEnumerable<Resource> GetAll()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in Model.GetITableFromDatabase(db)
                     select tableEntry).ToList();
            });
        }

        /// <summary>
        /// New structure:
        /// 
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}/?type=metadata
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}/?type=data
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}/?type=metadata&version={version}
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}/?type=data&version={version}
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}/?type=metadata&version={version}&extracted=true
        /// https://purl.org/coscine/resources/{GUID}/{*Folder}/{File}/?type=data&version={version}&extracted=true
        ///
        /// </summary>
        /// <param name="entries"></param>
        /// <returns></returns>
        public override IEnumerable<IGraph> ConvertToLinkedData(IEnumerable<Resource> entries)
        {
            var trellisGraph = RdfStoreConnector.GetEmptySmallUpdateGraph(Uris.TrellisGraph);
            var trellisIds = GetTrellisIds();

            var leftGraphs = new List<IGraph> { trellisGraph };
            var resourceUrlPrefix = "https://purl.org/coscine/resources";
            var resourceUrlHandlePrefix = $"https://hdl.handle.net/{Prefix}";

            var metadataGraphCreator = new MetadataGraphsCreator(RdfStoreConnector);

            var maxVersionScanning = long.Parse(
                Configuration.GetStringAndWait("coscine/global/metadata/migration/maxVersion", long.MaxValue + "")
            );

            var graphs = new List<IGraph>();

            foreach (var entry in entries)
            {
                var resourceGraphName = $"{resourceUrlPrefix}/{entry.Id}";
                var resourceHandleGraphName = $"{resourceUrlHandlePrefix}/{entry.Id}";

                var targetClass = GetTargetClass(entry);
                var fileGraphs = ListGraphsWithInstances(resourceHandleGraphName + "@", targetClass);

                foreach (var fileGraph in fileGraphs)
                {
                    if (fileGraph.AbsoluteUri != resourceHandleGraphName 
                        && fileGraph.AbsoluteUri.Contains("@path=") 
                        && !fileGraph.AbsoluteUri.Contains("&data")
                        && !fileGraph.AbsoluteUri.Contains("?type=")
                        && !fileGraph.AbsoluteUri.Contains("&type=")
                        && !fileGraph.AbsoluteUri.Contains("@type="))
                    {
                        Logger.LogInformation($"Migrating {fileGraph.AbsoluteUri}");

                        var fileGraphObject = RdfStoreConnector.GetGraph(fileGraph);
                        IGraph? linkedGraph = RdfStoreConnector.GetGraph(fileGraph.AbsoluteUri + "&data");
                        if (linkedGraph.IsEmpty)
                        {
                            linkedGraph = null;
                        }

                        var path = GetPath(fileGraph);

                        var version = VersionUtil.GetNewVersion();

                        var newFileGraphName = $"{resourceGraphName}{path}";
                        var newMetadataFileGraphName = $"{resourceGraphName}{path}/@type=metadata";
                        var newDataFileGraphName = $"{resourceGraphName}{path}/@type=data";
                        var newMetadataVersionFileGraphName = $"{resourceGraphName}{path}/@type=metadata&version={version}";
                        var newDataVersionFileGraphName = $"{resourceGraphName}{path}/@type=data&version={version}";

                        var existingGraphs = ListGraphsWithTrellisGraph(trellisIds, newFileGraphName + "/");

                        var notChanged = true;

                        var recentDataVersion = VersionUtil.GetRecentDataVersion(existingGraphs);
                        var currentDataVersionGraph = GetRecentVersionGraph(newDataVersionFileGraphName, recentDataVersion);

                        if (linkedGraph != null)
                        {
                            notChanged = notChanged && linkedGraph.Triples.All((triple) => currentDataVersionGraph.Triples.Any((otherTriple) =>
                                triple.Predicate.Equals(otherTriple.Predicate) &&
                                (triple.Object.Equals(otherTriple.Object) || (triple.Object.NodeType == NodeType.Blank && otherTriple.Object.NodeType == NodeType.Blank))));
                        }

                        var recentMetadataVersion = VersionUtil.GetRecentMetadataVersion(existingGraphs);
                        var recentMetadataVersionLong = VersionUtil.GetVersion(recentMetadataVersion);
                        var currentMetadataVersionGraph = GetRecentVersionGraph(newMetadataVersionFileGraphName, recentMetadataVersion);

                        if (fileGraphObject != null)
                        {
                            notChanged = notChanged && fileGraphObject.Triples.All((triple) => currentMetadataVersionGraph.Triples.Any((otherTriple) =>
                                triple.Predicate.Equals(otherTriple.Predicate) && 
                                (triple.Object.Equals(otherTriple.Object) || (triple.Object.NodeType == NodeType.Blank && otherTriple.Object.NodeType == NodeType.Blank))));
                        }

                        if (recentMetadataVersionLong != null && recentMetadataVersionLong > maxVersionScanning)
                        {
                            Logger.LogInformation($"New Version too recent (after migration)!");
                            continue;
                        }

                        if (notChanged)
                        {
                            Logger.LogInformation($"No change for {fileGraph.AbsoluteUri}, skipping!");
                            continue;
                        }

                        graphs.AddRange(
                            metadataGraphCreator.CreateGraphs(entry.Id.ToString(), path, true, true, new ProvidedGraphData { 
                                CurrentDataVersionGraph = currentDataVersionGraph,
                                CurrentMetadataVersionGraph = currentMetadataVersionGraph,

                                OldDataGraph = linkedGraph,
                                OldMetadataGraph = fileGraphObject,

                                RecentDataVersion = recentDataVersion,
                                RecentMetadataVersion = recentMetadataVersion,

                                TrellisGraph = trellisGraph 
                            })
                        );
                        graphs.RemoveAll((graph) => graph.BaseUri == Uris.TrellisGraph);
                    }

                    // Intermediate update to save memory
                    if (graphs.Count > 1000)
                    {
                        Logger.LogInformation("");
                        Logger.LogInformation("Intermediate Update");
                        Logger.LogInformation("");

                        StoreGraphs(graphs);
                        graphs.Clear();
                    }
                }
            }

            leftGraphs.AddRange(graphs);
            return leftGraphs;
        }


        private IGraph GetRecentVersionGraph(string newVersionFileGraphName, Uri? recentVersion)
        {
            if (recentVersion == null)
            {
                return new Graph()
                {
                    BaseUri = new Uri(newVersionFileGraphName),
                };
            }
            else
            {
                return RdfStoreConnector.GetGraph(recentVersion);
            }
        }

        private static string GetPath(Uri fileGraph)
        {
            var path = fileGraph.AbsoluteUri;
            path = path[(path.IndexOf("@path=") + "@path=".Length)..];
            path = path.Replace("%2F", "/");
            path = path.Replace("%2f", "/");
            return path;
        }

        /// <summary>
        /// Gets all trellis subject ids that are RDFSources.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Uri> GetTrellisIds()
        {
            Logger.LogInformation($"Getting Trellis Ids");

            var query = $@"WHERE {{ 
                GRAPH <http://www.trellisldp.org/ns/trellis#PreferServerManaged> {{ 
                    ?s a <http://www.w3.org/ns/ldp#RDFSource>  
                }}
            }}";

            var cmdString = new SparqlParameterizedString
            {
                CommandText = "SELECT COUNT(?s) AS ?count " + query
            };
            var result = BaseRdfStoreConnector.WrapRequest(() => RdfStoreConnector.QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            Logger.LogInformation($"{numberOfResult} Trellis Ids");

            var trellisIds = new List<Uri>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                Logger.LogInformation($"Getting {offset}/{numberOfResult} Trellis Ids");
                cmdString = new SparqlParameterizedString
                {
                    CommandText = "SELECT ?s " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}"
                };
                using var results = BaseRdfStoreConnector.WrapRequest(() => RdfStoreConnector.QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
                trellisIds.AddRange(results.Select(x => (x.Value("s") as IUriNode).Uri).ToList());
            }

            Logger.LogInformation($"Got all Trellis Ids");

            return trellisIds;
        }

        public IEnumerable<Uri> ListGraphsWithTrellisGraph(IEnumerable<Uri> trellisIds, string id)
        {
            return trellisIds.Where((trellisId) => trellisId.AbsoluteUri.Contains(id));
        }

        public IEnumerable<Uri> ListGraphsWithInstances(string id, string targetClass)
        {
            var cmdString = new SparqlParameterizedString
            {
                CommandText = @"SELECT DISTINCT ?g
                WHERE { GRAPH ?g { ?s a @targetClass }
                FILTER(contains(str(?g), @graph)) }"
            };
            cmdString.SetUri("targetClass", new Uri(targetClass));
            cmdString.SetLiteral("graph", id);

            var resultSet = BaseRdfStoreConnector.WrapRequest(() => RdfStoreConnector.QueryEndpoint.QueryWithResultSet(cmdString.ToString()));

            var graphs = new List<Uri>();
            foreach (SparqlResult r in resultSet)
            {
                var uriNode = r.Value("g") as IUriNode;
                if (uriNode != null)
                {
                    graphs.Add(uriNode.Uri);
                }
            }
            return graphs;
        }

        /// <summary>
        /// Since Application Profile URL does not have to be the targetClass, resolve it first
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private string GetTargetClass(Resource entry)
        {
            if (_targetClassMap.ContainsKey(entry.ApplicationProfile))
            {
                return _targetClassMap[entry.ApplicationProfile];
            }

            var targetClassCmdString = new SparqlParameterizedString
            {
                CommandText = "SELECT DISTINCT ?targetClass " +
                                "WHERE { @applicationProfile <http://www.w3.org/ns/shacl#targetClass> ?targetClass }"
            };
            targetClassCmdString.SetUri("applicationProfile", new Uri(entry.ApplicationProfile));
            var targetClassResultSet = BaseRdfStoreConnector.WrapRequest(() => RdfStoreConnector.QueryEndpoint.QueryWithResultSet(targetClassCmdString.ToString()));

            var targetClass = entry.ApplicationProfile;
            foreach (var result in targetClassResultSet)
            {
                targetClass = result[0].ToString();
            }

            _targetClassMap.Add(entry.ApplicationProfile, targetClass);

            return targetClass;
        }
    }
}
