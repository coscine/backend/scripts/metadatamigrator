﻿using MetadataMigrator.Implementations;

var dummyMode = !(args.Length > 0 && args[0] == "--noDryRun");
if (dummyMode)
{
    Console.WriteLine("\n DUMMY MODE \n");
    Console.WriteLine(" To exit dummy mode, execute with the \"--noDryRun\" argument");
}

Console.WriteLine("\nBegin MetadataMigrator migration");

var resourceStructuralData = new ResourceStructuralData();
resourceStructuralData.Migrate(dummyMode);

Console.WriteLine("\n Finished.");
