﻿using Coscine.Configuration;
using Coscine.Database.Models;
using Coscine.Metadata;
using VDS.RDF;
using MetadataMigrator.Logging;
using Microsoft.Extensions.Logging;
using NLog.Config;
using NLog.Extensions.Logging;

namespace MetadataMigrator
{
    public abstract class StructuralData<S, T> where S : class where T : DatabaseModel<S>, new()
    {
        public T Model { get; init; }
        public ConsulConfiguration Configuration { get; init; }
        public RdfStoreConnector RdfStoreConnector { get; init; }
        public static string? Prefix { get; set; }

        public ILogger Logger = null!;

        public StructuralData()
        {
            Configuration = new ConsulConfiguration();
            RdfStoreConnector = new RdfStoreConnector(Configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            Model = new T();
            Prefix = Configuration.GetStringAndWait("coscine/global/epic/prefix");
            // 100 second timeout
            var timeout = 100000;
            RdfStoreConnector.QueryEndpoint.Timeout = timeout;
            RdfStoreConnector.UpdateEndpoint.Timeout = timeout;
            RdfStoreConnector.ReadWriteSparqlConnector.Timeout = timeout;

            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("assembly-name", typeof(AssemblyNameLayoutRenderer));
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("assembly-version", typeof(AssemblyVersionLayoutRenderer));
            Logger = LoggerFactory.Create(builder => builder.AddNLog()).CreateLogger<StructuralData<S, T>>();
        }

        public abstract IEnumerable<IGraph> ConvertToLinkedData(IEnumerable<S> entries);

        public virtual IEnumerable<S> GetAll()
        {
            return Model.GetAll();
        }

        public void Migrate(bool dummyMode)
        {
            var spacer = new string('-', 35);
            Logger.LogInformation($"\n{spacer}\n{typeof(T).Name}\n{spacer}");
            var graphs = ConvertToLinkedData(GetAll());
            if (!dummyMode)
            {
                StoreGraphs(graphs);
            }
        }

        public void StoreGraphs(IEnumerable<IGraph> graphs)
        {
            foreach (var graph in graphs)
            {
                Logger.LogInformation($" ({graph.BaseUri})");

                RdfStoreConnector.AddGraph(graph);

                Logger.LogInformation($" - Graph {graph.BaseUri} added successfully");
                Logger.LogInformation("");
            }
        }
    }
}
